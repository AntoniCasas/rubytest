class AddUserToApiToken < ActiveRecord::Migration[5.2]
  def change
    add_reference :api_tokens, :user, foreign_key: true
  end
end
