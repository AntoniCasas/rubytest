class CreateIssues < ActiveRecord::Migration[5.2]
  def change
    create_table :issues do |t|
      t.string :title
      t.text :description
      t.date :created
      t.date :updated
      t.string :tipus, :default => 'BUG'
      t.string :priority, :default => 'TRIVIAL'

      t.timestamps
    end
  end
end
