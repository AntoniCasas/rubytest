class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :attachment_identifier

      t.timestamps
    end
  end
end
