class AddKillToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :kill, :string
  end
end
