class AddVotesToIssues < ActiveRecord::Migration[5.2]
  def change
    add_column :issues, :votes, :integer
  end
end
