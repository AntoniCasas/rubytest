class AddCallbackToSessionUser < ActiveRecord::Migration[5.2]
  def change
    add_column :session_users, :callback, :string
  end
end
