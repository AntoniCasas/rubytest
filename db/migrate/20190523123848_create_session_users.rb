class CreateSessionUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :session_users do |t|
      t.string :username
      t.integer :sessionId
      t.string :tok

      t.timestamps
    end
  end
end
