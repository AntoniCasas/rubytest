class AddAssignToIssue < ActiveRecord::Migration[5.2]
  def change
    add_reference :issues, :assign, foreign_key: true
  end
end
