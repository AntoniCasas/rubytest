# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_23_124309) do

  create_table "api_tokens", force: :cascade do |t|
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_api_tokens_on_user_id"
  end

  create_table "assigns", force: :cascade do |t|
    t.integer "issue_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["issue_id"], name: "index_assigns_on_issue_id"
    t.index ["user_id"], name: "index_assigns_on_user_id"
  end

  create_table "attachments", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer "user_id"
    t.integer "issue_id"
    t.string "text"
    t.date "created"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "kill"
    t.index ["issue_id"], name: "index_comments_on_issue_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "issues", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.date "created"
    t.date "updated"
    t.string "tipus"
    t.string "priority"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.string "status"
    t.integer "votes"
    t.integer "assign_id"
    t.string "stringassign"
    t.index ["assign_id"], name: "index_issues_on_assign_id"
    t.index ["user_id"], name: "index_issues_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "attachment_identifier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "comment_id"
    t.index ["comment_id"], name: "index_posts_on_comment_id"
  end

  create_table "session_users", force: :cascade do |t|
    t.string "username"
    t.integer "sessionId"
    t.string "tok"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "callback"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "avatar_url"
    t.string "email"
    t.string "uid"
    t.string "provider"
    t.string "oauth_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "post_id"
    t.integer "assign_id"
    t.integer "api_token_id"
    t.index ["api_token_id"], name: "index_users_on_api_token_id"
    t.index ["assign_id"], name: "index_users_on_assign_id"
    t.index ["post_id"], name: "index_users_on_post_id"
  end

  create_table "votes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "issue_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["issue_id"], name: "index_votes_on_issue_id"
    t.index ["user_id"], name: "index_votes_on_user_id"
  end

  create_table "watches", force: :cascade do |t|
    t.integer "user_id"
    t.integer "issue_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["issue_id"], name: "index_watches_on_issue_id"
    t.index ["user_id"], name: "index_watches_on_user_id"
  end

end
