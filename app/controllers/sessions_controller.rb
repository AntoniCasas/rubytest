class SessionsController < ApplicationController

  skip_before_action :authenticate_request, only: [:token]  
  def new
    redirect_to "/auth/github"
  end
  def login
    sessionid=params[:sessionId]
    cookies[:ses] = { :value =>sessionid, :expires => 1.hour.from_now }
    callback=params[:callback]
    ses=SessionUser.create()
    ses.sessionId=sessionid
    ses.callback=callback
    ses.save!
    params.delete :callback
    params.delete :sessionId
    redirect_to "/auth/github"
  end


  def create
    jsonWebToken=JsonWebToken.new()
    user = User.from_omniauth(request.env["omniauth.auth"])
    if user.valid?
      session[:user_id] = user.id
      if user.api_token.blank?
        token = JsonWebToken.encode(user_id: user.username) #enseñar esto a el usuario
        tok=ApiToken.new()
        tok.token=token
        tok.user=user
        tok.save
        user.api_token=tok
        user.save
      else
        token=user.api_token.token
            begin
              decoded_token = JWT.decode(token,Rails.application.secrets.secret_key_base)
            rescue JWT::ExpiredSignature
            token = JsonWebToken.encode(user_id: user.username) #enseñar esto a el usuario
            tok=ApiToken.new()
            user.api_token.destroy!
            tok.token=token
            tok.user=user
            tok.save
            user.api_token=tok
            end
          user.save
      end
      redirect_to "/"
    end
  end

  def red
    if (SessionUser.find_by_username(user.username)!=nil)
      se=SessionUser.find_by_username(user.username)
      sessionid=se.sessionId
      se.destroy!
      RestClient.get se.callback, {params: {sessionId: sessionid,token: token }}
    end
  end

  def destroy
    cookies.delete :_hello_world_session
    current_user = nil
    reset_session
    redirect_to "/"
  end
  
end
