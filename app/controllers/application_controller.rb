class ApplicationController < ActionController::Base
  #protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token
  helper_method :current_user
  before_action :authenticate_request

  def render_json_error(description, status)
    status = Rack::Utils::SYMBOL_TO_STATUS_CODE[status] if status.is_a? Symbol

    error = {
      description: description,
      status: status
    }

    render json: { error: [error] }, status: status
    false
  end
  
  private

  def authenticate_request
    if request.format.symbol == :json
        @current_user_api = AuthorizeApiRequest.call(request.headers).result
        if @current_user_api.blank?
            render_json_error('The authorization header is no present or is not valid.', :unauthorized)
        else 
            true
        end
    end
  end
  
  def current_user
    if @current_user_api.blank?
        session[:user_id].nil? ? nil : User.find(session[:user_id])
    else @current_user_api
    end
  end

end
