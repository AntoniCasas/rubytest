require 'date'
require 'carrierwave/orm/activerecord'

class IssuesController < ApplicationController
  before_action :set_issue, only: [:show, :edit, :update, :destroy, :vote, :unvote, :follow, :unfollow, :index_comment, :comment]
  before_action :set_comment, only: [:update_comment, :comment_destroy, :getonecomment]
  
  #POST /issues/1/vote
  def vote
    if !current_user.blank?
      if  @issue.vote.length()==0
      @issue.votes=@issue.votes+1
      user=User.find_by_username(current_user.username)
      vote= Vote.new()
      vote.issue=@issue
      vote.user=user
      user.vote.push(vote)
      @issue.vote.push(vote)
      @issue.save!
      vote.save!
      user.save!
      elsif (@issue.vote & User.find_by_username(current_user.username).vote).length()==0
        @issue.votes=@issue.votes+1
        user=User.find_by_username(current_user.username)
        vote= Vote.new()
        vote.issue=@issue
        vote.user=user
        user.vote.push(vote)
        @issue.vote.push(vote)
        @issue.save!
        vote.save!
        user.save!
      end
      if (request.format.symbol == :html)
        redirect_to :controller => 'issues', :action => 'show'
      end
      if (request.format.symbol == :json)
        
        render json: {"Message":"Vote successfully registered", status: '200'}
      end
    end
  end

  #POST /issues/1/follow
  def follow
    if  !current_user.blank?
      if  @issue.watch.length()==0
        user=User.find_by_username(current_user.username)
        watch= Watch.new()
        watch.issue=@issue
        watch.user=user
        user.watch.push(watch)
        @issue.watch.push(watch)
        @issue.save!
        watch.save!
        user.save!
      elsif (@issue.watch & User.find_by_username(current_user.username).watch).length()==0
        user=User.find_by_username(current_user.username)
        watch= Watch.new()
        watch.issue=@issue
        watch.user=user
        user.watch.push(watch)
        @issue.watch.push(watch)
        @issue.save!
        watch.save!
        user.save!
      end
      if (request.format.symbol == :html)
        redirect_to :controller => 'issues', :action => 'show'
      end
      if (request.format.symbol == :json)
        render json: {"Message":"Follow successfully registered", status: '200'}
      end
    end
  end

  #POST /issues/1/unfollow
  def unfollow
    if  !current_user.blank?
      if (@issue.watch & User.find_by_username(current_user.username).watch).length()>0
        user=User.find_by_username(current_user.username)
        vote=@issue.watch & user.watch
        user=User.find_by_username(current_user.username)
        for item in vote
          item.destroy
          user.watch.delete(item)
          @issue.watch.delete(item)
        end
        @issue.save!
        user.save!
      end
      if (request.format.symbol == :html)
        redirect_to :controller => 'issues', :action => 'show'
      end
      if (request.format.symbol == :json)
        render json: {"Message":"Unfollow successfully registered", status: '200'}
      end
    end
  end


  #POST /issues/1/unvote
  def unvote
    if  !current_user.blank?
      if (@issue.vote & User.find_by_username(current_user.username).vote).length()>0
        @issue.votes=@issue.votes-1
        user=User.find_by_username(current_user.username)
        vote=@issue.vote & user.vote
        for item in vote
          user.vote.delete(item)
          @issue.vote.delete(item)
          item.destroy!
        end
        @issue.save!
        user.save!
      end
      if (request.format.symbol == :html)
        redirect_to :controller => 'issues', :action => 'show'
      end
      if (request.format.symbol == :json)
        render json: {"Message":"Unvote successfully registered", status: '200'}
      end
    end
  end



  # GET /issues
  # GET /issues.json
  def index
    if (!current_user.blank?)
      sesi=cookies[:ses]
      cookies.delete :ses
    if (!sesi.blank? && !SessionUser.find_by_sessionId(sesi).blank?)
      puts "----------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------"
      se=SessionUser.find_by_sessionId(sesi)
      if (!se.blank?)
      callback=se.callback
      se.destroy!
      if (!callback.blank?)
      redirect_to callback+"?token="+current_user.api_token.token+"&username="+current_user.username
        end
      end
    end
    end
    @issues = Issue.where(nil)
    paramsFiltered=filtering_params(params)
    issuelist=Array.new
    if paramsFiltered.keys.include? "ordered_by_follow"
      user=User.find_by_username(current_user.username)
      watch=user.watch
      issuelist=Array.new
      for v in watch
        issuelist.push(v.issue)
      end
    end


    paramsFiltered.each do |key, value|
      @issues = @issues.public_send(key, value) if value.present?
    end
    if paramsFiltered.keys.include? "ordered_by_tipus"
      @issues = @issues.order("tipus asc")
    end
    if paramsFiltered.keys.include? "ordered_by_priority"
      @issues = @issues.order("priority asc")
    end
    if paramsFiltered.keys.include? "ordered_by_status"
      @issues = @issues.order("status asc")
    end
    if paramsFiltered.keys.include? "ordered_by_votes"
      @issues = @issues.order("votes desc")
    end
    if paramsFiltered.keys.include? "ordered_by_assignee"
      @issues = @issues.order("stringassign asc")
      end
    if paramsFiltered.keys.include? "ordered_by_created"
      @issues = @issues.order("created desc")
    end
    if paramsFiltered.keys.include? "ordered_by_updated"
      @issues = @issues.order("updated desc")
    end
    if paramsFiltered.keys.include? "ordered_by_title"
      @issues = @issues.order("title asc")
    end
    if paramsFiltered.keys.include? "ordered_by_follow"
      @issues=@issues & issuelist
    end
    
    if (request.format.symbol == :json) 
        issuelist = Array.new
        @issues.each do |issue|
          if !issue.nil?
            issuelist.push(get_issue_hal(issue))
            end
        end
        render json: issuelist, status: :ok
    end
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
      if (request.format.symbol == :json)
          render json: get_issue_hal(@issue), status: :ok
      end
  end

  # GET /issues/new
  def new
    if !current_user.username.blank?
        @issue = Issue.new()
        @issue.votes=0
        @issue.user =User.find_by_username(current_user.username)
    else 
        redirect_to getHostFromIssues+"/sessions/new"
    end
  end

  # GET /issues/1/edit
  def edit
  end
  
  # GET /issues/{issue_id}/comment
  def index_comment
      if (request.format.symbol == :json)
          commentlist = Array.new
          @issue.comment.each do |comment|
              commentlist.push(get_comment_hal(comment))
          end
          render json: commentlist, status: :ok
      end
  end
  
  # GET /issues/{issue_id}/comment/{comment_id}
  def getonecomment
      if !@issue.blank? && !@comment.blank?
          render json: get_comment_hal(@comment), status: :ok
      end
  end
  
  # GET /issues/{issue_id}/comment/{comment_id}
  def edit_comment
      @issue = Issue.find_by_id(params[:issue_id])
      @comment = Comment.find_by_id(params[:comment_id])
  end

  # POST /issues/new
  # POST /issues/new
  def create
    if (request.format.symbol == :html and check_enum_values(params[:issue][:status],params[:issue][:tipus],params[:issue][:priority]))
        ass=nil
        if !current_user.username.blank?
            @issue = Issue.new(issue_params.except(:name))
                us=User.find_by_username(params[:issue][:name])
                @issue.stringassign=us.username
            end
        elsif (request.format.symbol == :json and check_enum_values(params[:status],params[:tipus],params[:priority]))
          @issue = Issue.new()
          @issue.tipus=params[:tipus]
          @issue.priority=params[:priority]
          @issue.status=params[:status]
          @issue.title=params[:title]
          @issue.description=params[:description]
          if !params[:assigne].blank?

              us = User.find_by_username(params[:assigne]);
            if us.blank?
              us = User.find_by_username(current_user.username);
            end
          else
            us = User.find_by_username(current_user.username);
          end
          @issue.stringassign=us.username
    end

        @issue.votes=0
        @issue.user =User.find_by_username(current_user.username)
        d=Date.today
        @issue.updated=d
        @issue.created=d
        respond_to do |format|
        if @issue.save
            format.html { redirect_to @issue, notice: 'Issue was successfully created.' }
            format.json { render json: get_issue_hal(@issue), status: :ok }
        else
            format.html { render :new }
            format.json { render json: @issue.errors, status: :unprocessable_entity }
        end
        end
  end
  
  # POST /issues/{id}/comment
  def comment
    if !current_user.username.blank?
    @comment = Comment.new()
    if request.format.symbol == :html
        t = params[:comment][:text]
    else 
        t = params[:text]
    end
    if ((t.nil? or t.empty?) and request.format.symbol == :json)
        render_json_error('Text field is not defined', :bad_request)
    end
    @comment.text = t
    @comment.user = User.find_by_username(current_user.username)
    @comment.issue = @issue
    if request.format.symbol == :html
    if (params[:comment].include? "attachment_identifier")
      pos=Post.new()
      pos.attachment_identifier=params[:comment][:attachment_identifier]
      pos.comment=@comment
      pos.save!
      @comment.post.push(pos)
    end
    elsif request.format.symbol == :json
      pos=Post.new()
      pos.attachment_identifier=nil
      pos.comment=@comment
      pos.save!
    end
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @issue, notice: 'Comment was successfully created.' }
        format.json { render json: get_comment_hal(@comment), status: :created }
      else
        format.html { render :show }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
    end
    end


  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    if !current_user.username.blank? && ((Issue.find_by_id(params[:id]).user.username==User.find_by_username(current_user.username).username) || (Issue.find_by_id(params[:id]).stringassign==User.find_by_username(current_user.username).username))
      if (request.format.symbol == :html) or (request.format.symbol == :json and check_enum_values(params[:issue][:status],params[:issue][:tipus],params[:issue][:priority]))
      respond_to do |format|
        if !params[:issue][:name].blank?
          us=User.find_by_username(params[:issue][:name])
          us=User.find_by_username(params[:assigne])
          if (us.blank?)
            stringassign=current_user.username
          else
            stringassign=us.username
          end
          @issue.stringassign=stringassign
          @issue.save!
        elsif !params[:assigne].blank?
          us=User.find_by_username(params[:assigne])
          if (us.blank?)
            render_json_error('Assign does not exist', :not_found)
          end
          @issue.stringassign=us.username
          @issue.save!
        end
      old_issue = Issue.find_by_id(params[:id])
      if @issue.update(issue_params.except(:name))
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render json: get_issue_hal(@issue), status: :ok }
        d=Date.today
        @issue.updated=d
        elements_changed = []
        old_elements=[]
        new_elements=[]
        if old_issue.title != @issue.title
            elements_changed.push('title')
            old_elements.push(old_issue.title)
            new_elements.push(@issue.title)
        end
        if old_issue.description != @issue.description
            elements_changed.push('description')
            old_elements.push(old_issue.description)
            new_elements.push(@issue.description)

        end
        if old_issue.tipus != @issue.tipus
            elements_changed.push('type')
            old_elements.push(old_issue.tipus)
            new_elements.push(@issue.tipus)

        end
        if old_issue.priority != @issue.priority
            elements_changed.push('priority')
            old_elements.push(old_issue.priority)
            new_elements.push(@issue.priority)

        end
        if old_issue.status != @issue.status
            elements_changed.push('status')
            old_elements.push(old_issue.status)
            new_elements.push(@issue.status)

        end
        if old_issue.stringassign != @issue.stringassign
          elements_changed.push('assignee')
          old_elements.push(old_issue.stringassign)
          new_elements.push(@issue.stringassign)

        end
        if elements_changed.size != 0
            text = current_user.username + " changed the " + elements_changed[0] + " from " + old_elements[0] +" to " + new_elements[0]
            if elements_changed.size == 2
                text = text + " and " + elements_changed[1] +"  from " + old_elements[1] +" to " + new_elements[1]
            else
                count = 0
                elements_changed.each do |item|
                    if count==elements_changed.length()-1 && elements_changed.length>=2
                      text = text + " and " + item +"  from " + old_elements[count] +" to " + new_elements[count]
                    elsif count != 0
                        text = text + ", " + item +"  from " + old_elements[count] +" to " + new_elements[count]
                    end
                    count = count + 1
                end
            end
            text = text + " of this issue."
            comment = Comment.new()
            comment.issue = @issue
            comment.user = User.find_by_username(current_user.username)
            comment.text = text
            comment.save  
        end
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
        end
      end
    end
    else
        if (request.format.symbol == :json) 
            render_json_error('Only creator and assigned user are authorized to delete the issue', :forbidden)
        end
    end
  end

  # PATCH /issues/{issue_id}/comment/file/{comment_id}
  def upload_file
    if !current_user.username.blank? && Comment.find_by_id(params[:comment_id]).user==User.find_by_username(current_user.username)
      comment=Comment.find_by_id(params[:comment_id])
        if (!comment.post.blank?)
          for im in comment.post
            im.remove_attachment_identifier!
            im.destroy!
          end
        end
        pos=Post.new()
        pos.attachment_identifier=params[:file]
        pos.comment=comment
        pos.save!
        comment.post.push(pos)
        comment.save!
        pos.save!
        puts pos.attachment_identifier.url
      else
        render_json_error('Only the creator of the comment is allowed to modify the comment', :forbidden)
      end
    end

  # PATCH /issues/{issue_id}/comment/{comment_id}
  def update_comment
    if !current_user.username.blank? && Comment.find_by_id(params[:comment_id]).user==User.find_by_username(current_user.username)
      comment=Comment.find_by_id(params[:comment_id])
      if (request.format.symbol == :html)
      if (params[:comment].include? "attachment_identifier")
        for im in comment.post
          im.remove_attachment_identifier!
          im.destroy
        end
        pos=Post.new()
        pos.attachment_identifier=params[:comment][:attachment_identifier]
        comment.post.push(pos)
        pos.save!
      elsif  (request.format.symbol == :json)
        pos=comment.post
        pos.attachment_identifier=params[:comment][:attachment_identifier]
        pos.save!
        comment.post.push(pos)
      end
      end
      if (request.format.symbol != :json)
      if params[:comment][:kill]=="true"
        for im in comment.post
          im.remove_attachment_identifier!
          im.destroy
        end
      end
      end
      if (request.format.symbol != :json)
          text = params[:comment][:text]
      else 
          text = params[:text]
      end
      comment.text = text
      comment.save
      respond_to do |format|
        format.html { redirect_to @issue, notice: 'Comment was successfully updated.' }
        format.json { render json: get_comment_hal(comment), status: :ok }
    end
    else
        if (request.format.symbol == :json) 
            render_json_error('Only the creator is authorized to delete the comment', :forbidden)
        end
    end
    end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    if !current_user.username.blank? && ((Issue.find_by_id(params[:id]).user.username==User.find_by_username(current_user.username).username) || (Issue.find_by_id(params[:id]).stringassign==User.find_by_username(current_user.username).username))
      @issue.save
      @issue.destroy
      respond_to do |format|
        format.html { redirect_to issues_url, notice: 'Issue was successfully destroyed.' }
        format.json { render json: {}, status: :ok }
    end
    else
        if (request.format.symbol == :json) 
            render_json_error('Only creator and assigned user are authorized to delete the issue', :forbidden)
        end
    end
    end
    
  # DELETE /issues/{issue_id}/comment/{comment_id}
  def comment_destroy
    if !current_user.username.blank? && Comment.find_by_id(params[:comment_id]).user.username==current_user.username
        if @comment.issue.id.equal? @issue.id
            @comment.destroy!
        end
        respond_to do |format|
            format.html { redirect_to @issue, notice: 'Comment was successfully destroyed.' }
            format.json { render json: {}, status: :ok }
        end
        else
        if (request.format.symbol == :json) 
            render_json_error('Only the creator is authorized to delete the comment', :forbidden)
        end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      begin
        @issue = Issue.find(params[:id])
      rescue ActiveRecord::RecordNotFound => e
        if request.format.symbol == :json
            render_json_error('The issue with id ' + params[:id] + ' does not exist', :not_found)
        end
      end
    end
    
    def set_comment
      @issue = Issue.find_by_id(params[:issue_id])
      if request.format.symbol == :json && @issue.blank?
            render_json_error('The issue with id ' + params[:issue_id].to_s + ' does not exist', :not_found)
      end
      if !@issue.blank?
        begin
            @comment = Comment.find(params[:comment_id])
        rescue ActiveRecord::RecordNotFound => e
            if request.format.symbol == :json
                render_json_error('The comment with id ' + params[:comment_id].to_s + ' does not exist', :not_found)
            end
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def issue_params
      params.require(:issue).permit(:title, :name,:description, :tipus, :priority, :status, :body, :remove_attachment_identifier, :assign, :assigne)
    end
    
    def comment_params
        comment.require(:comment).permit(:text, :kill, :attachment_identifier, :title, :body, :remove_attachment_identifier)
    end

  private

  def filtering_params(params)
    params.slice(:tipus,:assign, :priority, :status ,:user, :follow, :ordered_by_created,  :ordered_by_updated,
                 :ordered_by_priority, :ordered_by_assignee, :ordered_by_status, :ordered_by_tipus,
                 :ordered_by_title, :ordered_by_votes, :ordered_by_follow)
  end

  def manage_filters(t, v)
      if request.original_url.include? t+'='
        return request.original_url
      elsif v == nil
        return "var is nil"
      else
        if request.original_url.include? '?'
          return request.original_url + '&' + t + '=' + v
        else
          return request.original_url + '?' + t + '=' + v
        end
    end
  end
  helper_method :manage_filters

  def manage_order(t)
    if request.original_url.include? "?ordered_by_"+t
      if request.original_url.include? "&"
        return request.original_url.gsub("ordered_by_"+t+"&", "")
      else
        return request.original_url.gsub("?ordered_by_"+t, "")
      end
    elsif request.original_url.include? "&ordered_by_"+t
      return request.original_url.gsub("&ordered_by_"+t, "")
    elsif request.original_url.include? "ordered_by_"
      return request.original_url.gsub(/ordered_by_\w+/, "ordered_by_" + t)
    else 
      if request.original_url.include? '?'
        return request.original_url + '&ordered_by_' + t
      else
        return request.original_url + '?ordered_by_' + t
      end
    end
end
helper_method :manage_order

  def getHostFromIssues()
    if request.original_url.include? 'issues'
      return request.original_url.split("issues")[0]
    elsif request.original_url.include? '?'
      return request.original_url.split("?")[0]
    else
      return request.original_url
    end
  end
  helper_method :getHostFromIssues

  def hasVoted()
    if @issue.vote.blank?
      return false
    else
    if  @issue.vote.length()==0
      return false
    elsif (@issue.vote & User.find_by_username(current_user.username).vote).length()==0
      return false
    else
      return true
    end
      end
  end
  helper_method :hasVoted

  def hasVotedCool(issue)
    if issue.vote.blank?
      return false
    else
      if  issue.vote.length()==0
        return false
      elsif (issue.vote & User.find_by_username(current_user.username).vote).length()==0
        return false
      else
        return true
      end
    end
  end
  helper_method :hasVotedCool


  def isFollowing(issue)
    if  !current_user.blank?
      if  issue.watch.length()==0
        return false
      elsif (issue.watch & User.find_by_username(current_user.username).watch).length()==0
        return false
      end
      return true
    end
  end
  helper_method :isFollowing
  
  #Check enum values
    def check_enum_values(status_value,type_value,priority_value)
        if !check_status(status_value) 
            render_json_error('The status attribute is not well defined. Check top description for more information.', :bad_request)
        else 
            if !check_type(type_value) 
                render_json_error('The tipus attribute is not well defined. Check top description for more information.', :bad_request)
            else 
                if !check_priority(priority_value) 
                    render_json_error('The priority attribute is not well defined. Check top description for more information.', :bad_request)
                else
                    true
                end
            end
        end
    end
    
    #Check if status value is correct
    def check_status(status_value)
        if !status_value.blank?
            accepted_elements = ["NEW","DUPLICATE", "RESOLVED", "INVALID", "ON HOLD"]
            accepted_elements.include? status_value 
        else true
        end
    end
    
    #Check if type value is correct
    def check_type(type_value)
        if !type_value.blank?
            accepted_elements = ["BUG", "ENHANCEMENT", "PROPOSAL", "TASK"]
            accepted_elements.include? type_value
        else true
        end
    end
    
    #Check if priority value is correct
    def check_priority(priority_value)
        if !priority_value.blank?
            accepted_elements = ["MAJOR", "TRIVIAL", "MINOR", "CRITICAL", "BLOCKER"]
            accepted_elements.include? priority_value
        else true
        end
    end
    
    #Return issue object with HAL 
    def get_issue_hal(issue) 
        issue_aux = {
                id: issue.id,
                title: issue.title,
                description: issue.description,
                created: issue.created,
                updated: issue.updated,
                status: issue.status,
                tipus: issue.tipus,
                priority: issue.priority,
                #created_at: issue.created_at,
                #updated_at: issue.updated_at,
                voted_by_user: hasVotedCool(issue),
                followed_by_user: isFollowing(issue),
                #user_id: issue.user.id,
                votes: issue.votes,
                #assign_id: issue.assign.id,
                _links: {
                         creator: {
                                href: '/users/'+issue.user.username
                         },
                         assign: {
                                href: '/users/'+issue.stringassign
                         }
                }
            }
        if !issue.comment.empty?
            comments_json = []
            issue.comment.each do |comment|
                comment_aux = {
                    href: '/issues/'+issue.id.to_s+'/comments/'+comment.id.to_s
                }
                comments_json.push(comment_aux)
            end
            comments_json = {
                comments: comments_json
            }
            issue_aux = issue_aux.merge(:_embedded => comments_json)
        end
        issue_aux
    end
    
    #Returns comment object with HAL
    def get_comment_hal(comment)
        comment_aux = {
            id: comment.id,
            #user_id: 7,
            #issue_id: 4,
            text: comment.text,
            #created: null,
            created_at: comment.created_at,
            updated_at: comment.updated_at,
            #kill: null
            _links: {
                         creator: {
                                href: '/users/'+comment.user.username
                         },
                         issue: {
                                href: '/issues/'+comment.issue.id.to_s
                         }
                }
        }
        if !comment.post.blank?
            attachment=comment.post.first.attachment_identifier.url
            attachment_json = {
                attachment: {
                    href: attachment
                }
            }
            if !attachment.blank? 
                comment_aux = comment_aux.merge(:_embedded => attachment_json)
            end
        end
        comment_aux
    end
end
