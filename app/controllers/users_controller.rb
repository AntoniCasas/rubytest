class UsersController < ApplicationController
    
  # GET /users/{username}
  def show
    user = User.find_by_username(params[:username])
    if !user.blank?
        render json: get_user(user), status: :ok
    else
        render_json_error('The user with name ' + params[:username] + ' does not exist', :not_found)
    end
  end

  def index
    user = User.where(nil)
    if !user.blank?
      userList = Array.new
      user.each do |us|
        userList.push(get_user(us))
      end
      render json: userList, status: :ok
    else
      render_json_error('No users exist', :not_found)
    end
  end
  
  private
  
  #Return user object
  def get_user(user) 
        user_aux = {
                #id: 2,
                username: user.username,
                avatar_url: user.avatar_url,
                email: user.email
                #uid: null,
                #provider: null,
                #oauth_token: null,
                #created_at: "2019-04-26T20:29:59.032Z",
                #updated_at: "2019-04-26T20:29:59.032Z",
                #post_id: null,
                #assign_id: null,
                #api_token_id: null
        }
   end
end
