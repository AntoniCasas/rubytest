class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :issue
  has_many :post, dependent: :delete_all
  scope :created_at, -> (created_at) { where created_at: created_at }

  validates :text, presence: true
  
end
