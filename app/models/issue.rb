class Issue < ApplicationRecord
  has_many :vote, dependent: :delete_all
  has_many :watch, dependent: :delete_all
  has_many :comment, dependent: :delete_all
  has_one :assign,  dependent: :destroy
  scope :tipus, -> (tipus) { where tipus: tipus }
  scope :priority, -> (priority) { where priority: priority }
  scope :status, -> (status) { where status: status }
  scope :assign, -> (user) { where stringassign: user}
  scope :follow, -> (user) { where watch: User.find_by_username(user).watch}

  belongs_to :user
  before_create :get_created

  validates :title, presence: true
  validates :description, presence: true


  def get_created
    self.created = Date.today
  end

end
