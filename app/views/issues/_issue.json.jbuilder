json.extract! issue, :id, :title, :description, :created, :updated, :tipus, :priority, :created_at, :updated_at
json.url issue_url(issue, format: :json)
