Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  resources :users, only: [:show], param: :username
  resources :attachments
  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  match '/sessions/getonetoken', to: 'sessions#token', via: 'get'
  match '/issues/:id/vote', to: 'issues#vote', via: 'post'
  match '/issues/:id/comments', to: 'issues#comment', via: 'post'
  match '/issues/:id/comments', to: 'issues#index_comment', via: 'get'
  match '/issues/:issue_id/comments/:comment_id', to: 'issues#comment_destroy', via: 'delete'
  match '/issues/:issue_id/comments/:comment_id', to: 'issues#getonecomment', via: 'get'
  match '/issues/:issue_id/comments/:comment_id', to: 'issues#update_comment', via: 'patch'
  match '/issues/:id/unvote', to: 'issues#unvote', via: 'post'
  match '/issues/:id/follow', to: 'issues#follow', via: 'post'
  match '/issues/:id/unfollow', to: 'issues#unfollow', via: 'post'
  match '/issues/new', to: 'issues#create', via: 'post'
  match '/issues/:issue_id/comments/file/:comment_id', to: 'issues#upload_file', via: 'patch'
  match '/issues/:issue_id/comments/:comment_id', to: 'issues#getonecomment', via: 'get'
  match '/users', to: 'users#index', via: 'get'
  match '/sessions/login', to: 'sessions#login', via: 'get'
  match '/sessions/red', to: 'sessions#red', via: 'get'

  resources :issues
  get "/auth/:provider/callback", to: "sessions#create"
  get 'auth/failure', to: redirect('/')
  delete 'signout', to: 'sessions#destroy', as: 'signout'
  root to: 'issues#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
