#README 

Membres de l'equip:

Ferran Agullo López

Antoni Casas i Muñoz

Rubén Gonzàlez López

Joan Marc Pastor Mackenzie

-----------------------------------------------------

Link a l'aplicació desplegada a Heroku:

https://blooming-forest-68248.herokuapp.com

-----------------------------------------------------

!!Important!!

La aplicació utilitza com a login el sistema de autorització extern de github, i la ip de callback està identificada com a la de Heroku, per a utilitzar aquesta aplicació localment, tindrás que definir un nou servei a github.
