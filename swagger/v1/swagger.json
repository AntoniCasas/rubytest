{
	"swagger": "2.0",
	"info": {
		"title": "Issue Tracker RestApi",
		"description": "Below is the IssueTracker API documentation",
		"version": "0.5",
		"license": {
			"name": "Apache 2.0",
			"url": "http://www.apache.org/licenses/LICENSE-2.0.html"
		}
	},
	"host": "blooming-forest-68248.herokuapp.com",
	"basePath": "/",
	"tags": [
		{
			"name": "Issues",
			"description": "Basic issues operations"
		},
		{
			"name": "Users",
			"description": "Users operations"
		},
		{
			"name": "Comments",
			"description": "Basic comments operations"
		},
		{
			"name": "Interactions",
			"description": "Interact with other users"
		}
	],
	"schemes": [
		"https"
	],
	"paths": {
		"/issues": {
			"get": {
				"summary": "Returns all the issues",
				"security": [
					{
						"Bearer": []
					}
				],
				"tags": [
					"Issues"
				],
				"produces": [
					"application/json"
				],
				"description": "Returns an array with all the saved issues ordered by the specified attributes.",
				"parameters": [
					{
						"name": "ordered_by_created",
						"in": "query",
						"type": "boolean",
						"description": "Order issues by time of creation"
					},
					{
						"name": "ordered_by_updated",
						"in": "query",
						"type": "boolean",
						"description": "Order issues by time of modification"
					},
					{
						"name": "ordered_by_priority",
						"in": "query",
						"type": "boolean",
						"description": "Order issues by priority"
					},
					{
						"name": "ordered_by_assignee",
						"in": "query",
						"type": "boolean",
						"description": "Order issues by assignee"
					},
					{
						"name": "ordered_by_status",
						"in": "query",
						"type": "boolean",
						"description": "Order issues by status"
					},
					{
						"name": "ordered_by_tipus",
						"in": "query",
						"type": "boolean",
						"description": "Order issues by tipus"
					},
					{
						"name": "ordered_by_title",
						"in": "query",
						"type": "boolean",
						"description": "Order issues by title"
					},
					{
						"name": "ordered_by_votes",
						"in": "query",
						"type": "boolean",
						"description": "Order issues by number of votes"
					},
					{
						"name": "ordered_by_follow",
						"in": "query",
						"type": "boolean",
						"description": "Show only followed issues"
					},
					{
						"name": "tipus",
						"in": "query",
						"type": "string",
						"enum": [
							"BUG",
							"ENHANCEMENT",
							"PROPOSAL",
							"TASK"
						],
						"description": "Show only issues that match with the following Tipus:"
					},
					{
						"name": "priority",
						"in": "query",
						"type": "string",
						"enum": [
							"MAJOR",
							"TRIVIAL",
							"MINOR",
							"CRITICAL",
							"BLOCKER"
						],
						"description": "Show only issues that match with the following Priority:"
					},
					{
						"name": "status",
						"in": "query",
						"type": "string",
						"enum": [
							"NEW",
							"DUPLICATE",
							"RESOLVED",
							"INVALID",
							"ON HOLD"
						],
						"description": "Show only issues that match with the following Status:"
					},
					{
						"name": "assign",
						"in": "query",
						"type": "string",
						"description": "Show only issues that match with the following Assignee:"
					}
				],
				"responses": {
					"200": {
						"description": "ok",
						"schema": {
							"type": "array",
							"items": {
								"$ref": "#/definitions/Issue"
							}
						}
					},
					"401": {
						"description": "The authorization header is no present or is not valid."
					}
				}
			},
			"post": {
				"summary": "Creates a issue",
				"security": [
					{
						"Bearer": []
					}
				],
				"tags": [
					"Issues"
				],
				"description": "Creates a new issue with the input information. An issue is defined by a title and a description wich are required attributes. Also has the following auxiliar parameters: status, _type and priority. The values that these attributes support are shown below: <li>status: NEW, DUPLICATE, RESOLVED, INVALID, ON HOLD </li> <li>_type: BUG, ENHANCEMENT, PROPOSAL, TASK</li>        <li>priority: MAJOR, TRIVIAL, MINOR, CRITICAL, BLOCKER</li>",
				"consumes": [
					"application/json"
				],
				"parameters": [
					{
						"name": "issue",
						"in": "body",
						"schema": {
							"$ref": "#/definitions/InputIssue"
						}
					}
				],
				"responses": {
					"201": {
						"description": "Issue created",
						"schema": {
							"$ref": "#/definitions/Issue"
						}
					},
					"400": {
						"description": "Bad request"
					},
					"401": {
						"description": "The authorization header is no present or is not valid."
					},
					"422": {
						"description": "Unprocessable entity"
					}
				}
			}
		},
		"/issues/{id}": {
			"get": {
				"summary": "Returns an issue",
				"security": [
					{
						"Bearer": []
					}
				],
				"tags": [
					"Issues"
				],
				"produces": [
					"application/json"
				],
				"description": "Returns the issue identified by the path variable.",
				"parameters": [
					{
						"name": "id",
						"in": "path",
						"type": "integer",
						"required": true
					}
				],
				"responses": {
					"200": {
						"description": "ok",
						"schema": {
							"$ref": "#/definitions/Issue"
						}
					},
					"401": {
						"description": "The authorization header is no present or is not valid."
					},
					"404": {
						"description": "The issue does not exist"
					}
				}
			},
			"patch": {
				"summary": "Updates a issue",
				"security": [
					{
						"Bearer": []
					}
				],
				"tags": [
					"Issues"
				],
				"description": "Creates a new issue with the input information. An issue is defined by a title and a description wich are required attributes. Also has the following auxiliar parameters: status, _type and priority. The values that these attributes support are shown below: <li>status: NEW, DUPLICATE, RESOLVED, INVALID, ON HOLD </li> <li>_type: BUG, ENHANCEMENT, PROPOSAL, TASK</li>        <li>priority: MAJOR, TRIVIAL, MINOR, CRITICAL, BLOCKER</li>",
				"consumes": [
					"application/json"
				],
				"parameters": [
					{
						"name": "id",
						"in": "path",
						"type": "integer",
						"required": true
					},
					{
						"name": "issue",
						"in": "body",
						"schema": {
							"$ref": "#/definitions/InputIssue"
						}
					}
				],
				"responses": {
					"200": {
						"description": "Issue updated",
						"schema": {
							"$ref": "#/definitions/Issue"
						}
					},
					"401": {
						"description": "The authorization header is no present or is not valid"
					},
					"403": {
						"description": "Only creator and assigned user are authorized to update the issue"
					},
					"404": {
						"description": "The issue does not exist"
					},
					"422": {
						"description": "Unprocessable entity"
					}
				}
			},
			"delete": {
				"summary": "Deletes a issue",
				"security": [
					{
						"Bearer": []
					}
				],
				"tags": [
					"Issues"
				],
				"produces": [
					"application/json"
				],
				"description": "Deletes the issue identified by the path variable.",
				"parameters": [
					{
						"name": "id",
						"in": "path",
						"type": "integer",
						"required": true
					}
				],
				"responses": {
					"200": {
						"description": "Issue deleted"
					},
					"401": {
						"description": "The authorization header is no present or is not valid"
					},
					"403": {
						"description": "Only creator and assigned user are authorized to delete the issue"
					},
					"404": {
						"description": "The issue does not exist"
					}
				}
			}
		},
		"/users": {
			"get": {
				"summary": "Returns all the users",
				"security": [
					{
						"Bearer": []
					}
				],
				"tags": [
					"Users"
				],
				"produces": [
					"application/json"
				],
				"description": "Returns all the users.",
				"parameters": [],
				"responses": {
					"200": {
						"description": "ok",
						"schema": {
							"type": "array",
							"items": {
								"$ref": "#/definitions/User"
							}
						},
						"401": {
							"description": "The authorization header is no present or is not valid."
						},
						"404": {
							"description": "The user does not exist"
						}
					}
				}
			}
		},
			"/users/{username}": {
				"get": {
					"summary": "Returns a user",
					"security": [
						{
							"Bearer": []
						}
					],
					"tags": [
						"Users"
					],
					"produces": [
						"application/json"
					],
					"description": "Returns the user identified by the path variable.",
					"parameters": [
						{
							"name": "username",
							"in": "path",
							"type": "string",
							"required": true
						}
					],
					"responses": {
						"200": {
							"description": "ok",
							"schema": {
								"$ref": "#/definitions/User"
							}
						},
						"401": {
							"description": "The authorization header is no present or is not valid."
						},
						"404": {
							"description": "The user does not exist"
						}
					}
				}
			},
			"/issues/{id}/comments": {
				"get": {
					"summary": "Returns all the comments of the specified issue",
					"security": [
						{
							"Bearer": []
						}
					],
					"tags": [
						"Comments"
					],
					"description": "Returns all the comments of the specified issue.",
					"produces": [
						"application/json"
					],
					"parameters": [
						{
							"name": "id",
							"in": "path",
							"type": "integer",
							"required": true
						}
					],
					"responses": {
						"200": {
							"description": "ok",
							"schema": {
								"$ref": "#/definitions/Comment"
							}
						},
						"401": {
							"description": "The authorization header is no present or is not valid."
						},
						"404": {
							"description": "The issue does not exist"
						}
					}
				},
				"post": {
					"summary": "Creates a comment",
					"security": [
						{
							"Bearer": []
						}
					],
					"tags": [
						"Comments"
					],
					"description": "Creates a new comment of an specified issue.",
					"parameters": [
						{
							"name": "id",
							"in": "path",
							"type": "integer",
							"required": true
						},
						{
							"name": "comment",
							"in": "body",
							"schema": {
								"$ref": "#/definitions/InputComment"
							}
						}
					],
					"consumes": [
						"application/json"
					],
					"responses": {
						"201": {
							"description": "Comment created",
							"schema": {
								"$ref": "#/definitions/Comment"
							}
						},
						"401": {
							"description": "The authorization header is no present or is not valid."
						},
						"404": {
							"description": "The issue does not exist"
						},
						"422": {
							"description": "Unprocessable entity"
						}
					}
				}
			},
			"/issues/{issue_id}/comments/{comment_id}": {
				"get": {
					"summary": "Returns a comment",
					"security": [
						{
							"Bearer": []
						}
					],
					"tags": [
						"Comments"
					],
					"description": "Returns the comment indentified by the two input identifiers",
					"parameters": [
						{
							"name": "issue_id",
							"in": "path",
							"type": "integer",
							"required": true
						},
						{
							"name": "comment_id",
							"in": "path",
							"type": "integer",
							"required": true
						}
					],
					"responses": {
						"200": {
							"description": "Returns the comment",
							"schema": {
								"$ref": "#/definitions/Comment"
							}
						},
						"401": {
							"description": "The authorization header is no present or is not valid"
						},
						"404": {
							"description": "The comment or the issue do not exist"
						},
						"422": {
							"description": "Unprocessable entity"
						}
					}
				},
				"patch": {
					"summary": "Updates a comment",
					"security": [
						{
							"Bearer": []
						}
					],
					"tags": [
						"Comments"
					],
					"description": "Updates the comment identified by the path variables.",
					"consumes": [
						"application/json"
					],
					"parameters": [
						{
							"name": "issue_id",
							"in": "path",
							"type": "integer",
							"required": true
						},
						{
							"name": "comment_id",
							"in": "path",
							"type": "integer",
							"required": true
						},
						{
							"name": "comment",
							"in": "body",
							"schema": {
								"$ref": "#/definitions/InputComment"
							}
						}
					],
					"responses": {
						"200": {
							"description": "Comment updated",
							"schema": {
								"$ref": "#/definitions/Comment"
							}
						},
						"401": {
							"description": "The authorization header is no present or is not valid"
						},
						"403": {
							"description": "Only the creator is authorized to delete the comment"
						},
						"404": {
							"description": "The comment does not exist"
						},
						"422": {
							"description": "Unprocessable entity"
						}
					}
				},
					"delete": {
						"summary": "Deletes a comment",
						"security": [
							{
								"Bearer": []
							}
						],
						"tags": [
							"Comments"
						],
						"produces": [
							"application/json"
						],
						"description": "Deletes the comment identified by the path variables.",
						"parameters": [
							{
								"name": "issue_id",
								"in": "path",
								"type": "integer",
								"required": true
							},
							{
								"name": "comment_id",
								"in": "path",
								"type": "integer",
								"required": true
							}
						],
						"responses": {
							"200": {
								"description": "Comment deleted"
							},
							"401": {
								"description": "The authorization header is no present or is not valid"
							},
							"403": {
								"description": "Only the creator is authorized to delete the comment"
							},
							"404": {
								"description": "The comment does not exist"
							}
						}
					}
				},
				"/uploads/post/attachment_identifier/{fileId}/{filename}": {
					"get": {
						"summary": "Returns the attachment specified by its fileId and filename.",
						"security": [
							{
								"Bearer": []
							}
						],
						"tags": [
							"Comments"
						],
						"description": "Returns the attachment specified by its fileId and filename.",
						"produces": [
							"application/octet-stream"
						],
						"parameters": [
							{
								"name": "fileId",
								"in": "path",
								"type": "integer",
								"required": true
							},
							{
								"name": "filename",
								"in": "path",
								"type": "string",
								"required": true
							}
						],
						"responses": {
							"200": {
								"description": "ok",
								"schema": {
									"$ref": "#"
								}
							},
							"401": {
								"description": "The authorization header is no present or is not valid."
							},
							"404": {
								"description": "The attachment does not exist"
							}
						}
					}
				},
				"/issues/{issue_id}/comments/file/{comment_id}": {
					"patch": {
						"summary": "Add or modify an attachment of a comment",
						"security": [
							{
								"Bearer": []
							}
						],
						"tags": [
							"Comments"
						],
						"description": "Add or modify an attachment of a comment.",
						"consumes": [
							"multipart/form-data"
						],
						"parameters": [
							{
								"name": "issue_id",
								"in": "path",
								"type": "integer",
								"required": true
							},
							{
								"name": "comment_id",
								"in": "path",
								"type": "integer",
								"required": true
							},
							{
								"in": "formData",
								"name": "file",
								"type": "file",
								"description": "The file to upload."
							}
						],
						"responses": {
							"200": {
								"description": "Comment updated"
							},
							"401": {
								"description": "The authorization header is no present or is not valid"
							},
							"403": {
								"description": "Only the creator is authorized to delete the comment"
							},
							"404": {
								"description": "The comment does not exist"
							},
							"422": {
								"description": "Unprocessable entity"
							}
						}
					}
				},
				"/issues/{id}/follow": {
					"post": {
						"summary": "Follow an specified issue",
						"security": [
							{
								"Bearer": []
							}
						],
						"tags": [
							"Interactions"
						],
						"description": "Follows the issue with the specified id.",
						"produces": [
							"application/json"
						],
						"parameters": [
							{
								"name": "id",
								"in": "path",
								"type": "integer",
								"required": true
							}
						],
						"responses": {
							"200": {
								"description": "ok",
								"schema": {
									"type": "array",
									"items": {
										"properties": {
											"title": {
												"type": "string"
											},
											"description": {
												"type": "string"
											},
											"status": {
												"type": "string"
											},
											"_type": {
												"type": "string"
											},
											"priority": {
												"type": "string"
											}
										}
									}
								}
							},
							"401": {
								"description": "The authorization header is no present or is not valid."
							}
						}
					}
				},
				"/issues/{id}/unfollow": {
					"post": {
						"summary": "Unfollow an specified issue",
						"security": [
							{
								"Bearer": []
							}
						],
						"tags": [
							"Interactions"
						],
						"description": "Unfollows the issue with the specified id.",
						"produces": [
							"application/json"
						],
						"parameters": [
							{
								"name": "id",
								"in": "path",
								"type": "integer",
								"required": true
							}
						],
						"responses": {
							"200": {
								"description": "ok",
								"schema": {
									"type": "array",
									"items": {
										"properties": {
											"title": {
												"type": "string"
											},
											"description": {
												"type": "string"
											},
											"status": {
												"type": "string"
											},
											"_type": {
												"type": "string"
											},
											"priority": {
												"type": "string"
											}
										}
									}
								}
							},
							"401": {
								"description": "The authorization header is no present or is not valid."
							}
						}
					}
				},
				"/issues/{id}/vote": {
					"post": {
						"summary": "Vote an specified issue",
						"security": [
							{
								"Bearer": []
							}
						],
						"tags": [
							"Interactions"
						],
						"description": "Upvotes the issue with the specified id.",
						"produces": [
							"application/json"
						],
						"parameters": [
							{
								"name": "id",
								"in": "path",
								"type": "integer",
								"required": true
							}
						],
						"responses": {
							"200": {
								"description": "ok",
								"schema": {
									"type": "array",
									"items": {
										"properties": {
											"title": {
												"type": "string"
											},
											"description": {
												"type": "string"
											},
											"status": {
												"type": "string"
											},
											"_type": {
												"type": "string"
											},
											"priority": {
												"type": "string"
											}
										}
									}
								}
							},
							"401": {
								"description": "The authorization header is no present or is not valid."
							}
						}
					}
				},
				"/issues/{id}/unvote": {
					"post": {
						"summary": "Unvote an specified issue",
						"security": [
							{
								"Bearer": []
							}
						],
						"tags": [
							"Interactions"
						],
						"description": "Unvotes the issue with the specified id.",
						"produces": [
							"application/json"
						],
						"parameters": [
							{
								"name": "id",
								"in": "path",
								"type": "integer",
								"required": true
							}
						],
						"responses": {
							"200": {
								"description": "ok",
								"schema": {
									"type": "array",
									"items": {
										"properties": {
											"title": {
												"type": "string"
											},
											"description": {
												"type": "string"
											},
											"status": {
												"type": "string"
											},
											"_type": {
												"type": "string"
											},
											"priority": {
												"type": "string"
											}
										}
									}
								}
							},
							"401": {
								"description": "The authorization header is no present or is not valid."
							}
						}
					}
				}
			},
			"securityDefinitions": {
				"Bearer": {
					"description": "JWT Authorization Token",
					"type": "apiKey",
					"name": "Authorization",
					"in": "header"
				}
			},
			"definitions": {
				"Issue": {
					"type": "object",
					"properties": {
						"id": {
							"type": "integer",
							"format": "int64"
						},
						"title": {
							"type": "string"
						},
						"description": {
							"type": "string"
						},
						"created_at": {
							"type": "string"
						},
						"updated_at": {
							"type": "string"
						},
						"status": {
							"type": "string",
							"enum": [
								"NEW",
								"DUPLICATE",
								"RESOLVED",
								"INVALID",
								"ON HOLD"
							]
						},
						"tipus": {
							"type": "string",
							"enum": [
								"BUG",
								"ENHANCEMENT",
								"PROPOSAL",
								"TASK"
							]
						},
						"priority": {
							"type": "string",
							"enum": [
								"MAJOR",
								"TRIVIAL",
								"MINOR",
								"CRITICAL",
								"BLOCKER"
							]
						},
						"votes": {
							"type": "integer",
							"format": "int64"
						},
						"_links": {
							"type": "object",
							"properties": {
								"creator": {
									"type": "object",
									"properties": {
										"href": {
											"type": "string"
										}
									}
								},
								"assign": {
									"type": "object",
									"properties": {
										"href": {
											"type": "string"
										}
									}
								}
							}
						},
						"_embedded": {
							"type": "object",
							"properties": {
								"comments": {
									"type": "array",
									"items": {
										"type": "object",
										"properties": {
											"href": {
												"type": "string"
											}
										}
									}
								}
							}
						}
					}
				},
				"InputIssue": {
					"type": "object",
					"required": [
						"title",
						"description"
					],
					"properties": {
						"title": {
							"type": "string",
							"example": "Rswag needs update"
						},
						"description": {
							"type": "string",
							"example": "The current version of rswag doesn't use the last swagger version"
						},
						"status": {
							"type": "string",
							"example": "NEW",
							"enum": [
								"NEW",
								"DUPLICATE",
								"RESOLVED",
								"INVALID",
								"ON HOLD"
							]
						},
						"tipus": {
							"type": "string",
							"example": "ENHANCEMENT",
							"enum": [
								"BUG",
								"ENHANCEMENT",
								"PROPOSAL",
								"TASK"
							]
						},
						"priority": {
							"type": "string",
							"example": "TRIVIAL",
							"enum": [
								"MAJOR",
								"TRIVIAL",
								"MINOR",
								"CRITICAL",
								"BLOCKER"
							]
						},
						"assigne": {
							"type": "string",
							"example": ""
						}
					}
				},
				"User": {
					"type": "object",
					"properties": {
						"username": {
							"type": "string"
						},
						"avatar_url": {
							"type": "string"
						},
						"email": {
							"type": "string"
						}
					}
				},
				"Comment": {
					"type": "object",
					"properties": {
						"id": {
							"type": "integer",
							"format": "int64"
						},
						"text": {
							"type": "string"
						},
						"created_at": {
							"type": "string"
						},
						"updated_at": {
							"type": "string"
						},
						"_links": {
							"type": "object",
							"properties": {
								"creator": {
									"type": "object",
									"properties": {
										"href": {
											"type": "string"
										}
									}
								},
								"issue": {
									"type": "object",
									"properties": {
										"href": {
											"type": "string"
										}
									}
								}
							}
						},
						"_embedded": {
							"type": "object",
							"properties": {
								"attachment": {
									"type": "object",
									"properties": {
										"href": {
											"type": "string"
										}
									}
								}
							}
						}
					}
				},
				"InputComment": {
					"type": "object",
					"required": [
						"text"
					],
					"properties": {
						"text": {
							"type": "string"
						}
					}
				}
			}
		}


