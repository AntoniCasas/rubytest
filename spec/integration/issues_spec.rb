require 'swagger_helper'

describe 'Issue Tracker API' do
    
  #ISSUES
    
  #index issue
  path '/issues' do

    get 'Returns all the issues' do
      security [Bearer: []]
      tags 'Issues'
      produces 'application/json'
      description 'Returns an array with all the saved issues ordered by the specified attributes.'
      parameter name: :ordered_by_created, type: :boolean
      parameter name: :ordered_by_updated, type: :boolean
      parameter name: :ordered_by_priority, type: :boolean
      parameter name: :ordered_by_assignee, type: :boolean
      parameter name: :ordered_by_status, type: :boolean
      parameter name: :ordered_by_tipus, type: :boolean
      parameter name: :ordered_by_title, type: :boolean
      parameter name: :ordered_by_votes, type: :boolean
      parameter name: :ordered_by_follow, type: :boolean

      response '200', 'ok' do
        schema type: :array,
        items: {
          properties: {
            id: { type: :integer },
            title: { type: :string },
            description: { type: :string },
            created_at: { type: :string },
            updated_at: { type: :string },
            status: { type: :string },
            tipus: { type: :string },
            priority: { type: :string },
            votes: { type: :integer },
            _links: {
                     type: :object,
                     properties: {
                                  creator: {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           },
                                  assign:  {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           }
                                 }
                    }
          }
        }

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
    end
  end
    
  #show issue
  path '/issues/{id}' do

    get 'Returns a issue' do
      security [Bearer: []]
      tags 'Issues'
      produces 'application/json'
      description 'Returns the issue identified by the path variable.'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'ok' do
        schema type: :object,
          properties: {
            id: { type: :integer },
            title: { type: :string },
            description: { type: :string },
            created_at: { type: :string },
            updated_at: { type: :string },
            status: { type: :string },
            tipus: { type: :string },
            priority: { type: :string },
            votes: { type: :integer },
            _links: {
                     type: :object,
                     properties: {
                                  creator: {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           },
                                  assign:  {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           }
                                 }
                    }
          }

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end

      response '404', 'The issue does not exist' do
        let(:id) { 'invalid' }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
    end
  end

  #create issue
  path '/issues' do

    post 'Creates a issue' do
      security [Bearer: []]
      tags 'Issues'
      description '<p>Creates a new issue with the input information. An issue is defined by a title and a description wich are required attributes. Also has the following auxiliary parameters: status, _type and priority. The values that these attributes support are shown below:</p>
      <ul><li>status: NEW,DUPLICATE, RESOLVED, INVALID, ON HOLD</li>
      <li>_type: BUG, ENHANCEMENT, PROPOSAL, TASK</li> 
      <li>priority: MAJOR, TRIVIAL, MINOR, CRITICAL, BLOCKER</li></ul>'
      consumes 'application/json'
      parameter name: :issue, in: :body, schema: {
        type: :object,
        properties: {
          title: { type: :string },
          description: { type: :string },
          status: { type: :string },
          tipus: { type: :string },
          priority: { type: :string }
        },
        required: [ 'title', 'description' ]
      }

      response '201', 'Issue created' do
          schema type: :object,
          properties: {
            id: { type: :integer },
            title: { type: :string },
            description: { type: :string },
            created_at: { type: :string },
            updated_at: { type: :string },
            status: { type: :string },
            tipus: { type: :string },
            priority: { type: :string },
            votes: { type: :integer },
            _links: {
                     type: :object,
                     properties: {
                                  creator: {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           },
                                  assign:  {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           }
                                 }
                    }
          }
        let(:issue) { { title: 'Dodo', description: 'available' } }
        run_test!
      end
      
      response '400', 'Bad request' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end

      response '422', 'Unprocessable entity' do
        let(:issue) { { title: 'foo' } }
        run_test!
      end
    end
  end
  
  #update issue
  path '/issues/{id}' do

    patch 'Updates a issue' do
      security [Bearer: []]
      tags 'Issues'
      description 'Updates the issue identified by the path variable. Check the create issue method description to find out the definition of the different parameters.'
      consumes 'application/json'
      parameter name: :id, :in => :path, :type => :integer
      parameter name: :issue, in: :body, schema: {
        type: :object,
        properties: {
          title: { type: :string },
          description: { type: :string },
          status: { type: :string },
          tipus: { type: :string },
          priority: { type: :string }
        }
      }

      response '200', 'Issue updated' do
          schema type: :object,
          properties: {
            id: { type: :integer },
            title: { type: :string },
            description: { type: :string },
            created_at: { type: :string },
            updated_at: { type: :string },
            status: { type: :string },
            tipus: { type: :string },
            priority: { type: :string },
            votes: { type: :integer },
            _links: {
                     type: :object,
                     properties: {
                                  creator: {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           },
                                  assign:  {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           }
                                 }
                    }
          }
        let(:issue) { { title: 'Dodo', description: 'available' } }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '403', 'Only creator and assigned user are authorized to update the issue' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '404', 'The issue does not exist' do
        let(:id) { 'invalid' }
        run_test!
      end

      response '422', 'Unprocessable entity' do
        let(:issue) { { title: 'foo' } }
        run_test!
      end
    end
  end
  
  #delete issue
  path '/issues/{id}' do

    delete 'Deletes a issue' do
      security [Bearer: []]
      tags 'Issues'
      produces 'application/json'
      description 'Deletes the issue identified by the path variable.'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'Issue deleted' do

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '403', 'Only creator and assigned user are authorized to delete the issue' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end

      response '404', 'The issue does not exist' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
  
  
  
  #USERS
  
  #create
  path '/sessions/getonetoken' do

    get 'Returns a token' do
      tags 'Users'
      description 'Returns auth web token.'    

      response '200', 'Ok' do
        let(:issue) { { title: 'Dodo', description: 'available' } }
        run_test!
      end
    
    end
  end
  
  #show issue
  path '/users/{username}' do

    get 'Returns a user' do
      security [Bearer: []]
      tags 'Users'
      produces 'application/json'
      description 'Returns the user identified by the path variable.'
      parameter name: :username, :in => :path, :type => :string

      response '200', 'ok' do
        schema type: :object,
          properties: {
            username: { type: :string },
            avatar_url: { type: :string },
            email: { type: :string }
          }

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end

      response '404', 'The user does not exist' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
  
  
  
  #COMMENTS
  
  #index_comment
  path '/issues/{id}/comment' do

    get 'Returns all the comments of the specified issue' do
      security [Bearer: []]
      tags 'Comments'
      description 'Returns all the comments of the specified issue.'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'ok' do
        schema type: :array,
        items: {
          properties: {
            id: { type: :integer },
            text: { type: :string },
            created_at: { type: :string },
            updated_at: { type: :string },
            _links: {
                     type: :object,
                     properties: {
                                  creator: {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           },
                                  issue:  {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           }
                                 }
                    }
          }
        }

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '404', 'The issue does not exist' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
  
  #create_comment
  path '/issues/{id}/comment' do

    post 'Creates a comment' do
      security [Bearer: []]
      tags 'Comments'
      description 'Creates a new comment of an specified issue.'
      parameter name: :id, :in => :path, :type => :integer
      parameter name: :comment, in: :body, schema: {
        type: :object,
        properties: {
          text: { type: :string }
        },
        required: [ 'comment' ]
      }
      consumes 'application/json'

      response '201', 'Comment created' do
          schema type: :object,
          properties: {
            id: { type: :integer },
            text: { type: :string },
            created_at: { type: :string },
            updated_at: { type: :string },
            _links: {
                     type: :object,
                     properties: {
                                  creator: {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           },
                                  issue:  {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           }
                                 }
                    }
          }
          
        let(:issue) { { title: 'Dodo', description: 'available' } }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '404', 'The issue does not exist' do
        let(:id) { 'invalid' }
        run_test!
      end

      response '422', 'Unprocessable entity' do
        let(:issue) { { title: 'foo' } }
        run_test!
      end
    end
  end
  
  #update comment
  path '/issues/{issue_id}/comment/{comment_id}' do

    patch 'Updates a comment' do
      security [Bearer: []]
      tags 'Comments'
      description 'Updates the comment identified by the path variables.'
      consumes 'application/json'
      parameter name: :issue_id, :in => :path, :type => :integer
      parameter name: :comment_id, :in => :path, :type => :integer
      parameter name: :comment, in: :body, schema: {
        type: :object,
        properties: {
          text: { type: :string }
        }
      },
      required: [ 'comment' ]

      response '200', 'Comment updated' do
          schema type: :object,
          properties: {
            id: { type: :integer },
            text: { type: :string },
            created_at: { type: :string },
            updated_at: { type: :string },
            _links: {
                     type: :object,
                     properties: {
                                  creator: {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           },
                                  issue:  {
                                            type: :object,
                                            properties: {
                                                        href: {type: :string}
                                                        }
                                           }
                                 }
                    }
          }
          
        let(:issue) { { title: 'Dodo', description: 'available' } }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '403', 'Only the creator is authorized to delete the comment' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '404', 'The comment does not exist' do
        let(:id) { 'invalid' }
        run_test!
      end

      response '422', 'Unprocessable entity' do
        let(:issue) { { title: 'foo' } }
        run_test!
      end
    end
  end
  
  #delete comment
  path '/issues/{issue_id}/comment/{comment_id}' do

    delete 'Deletes a comment' do
      security [Bearer: []]
      tags 'Comments'
      produces 'application/json'
      description 'Deletes the comment identified by the path variables.'
      parameter name: :issue_id, :in => :path, :type => :integer
      parameter name: :comment_id, :in => :path, :type => :integer

      response '200', 'Comment deleted' do

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
      
      response '403', 'Only the creator is authorized to delete the comment' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end

      response '404', 'The comment does not exist' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
  
  
  #INTERACTIONS
  
  #follow
  path 'issues/{id}/follow' do

    post 'Follow an specified issue' do
      security [Bearer: []]
      tags 'Interactions'
      description 'Not implemented yet.'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'ok' do
        schema type: :array,
        items: {
          properties: {
            title: { type: :string },
            description: { type: :string },
            status: { type: :string },
            _type: { type: :string },
            priority: { type: :string }
          }
        }

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
    end
  end
  
  #unfollow
  path 'issues/{id}/unfollow' do

    post 'Unfollow an specified issue' do
      security [Bearer: []]
      tags 'Interactions'
      description 'Not implemented yet.'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'ok' do
        schema type: :array,
        items: {
          properties: {
            title: { type: :string },
            description: { type: :string },
            status: { type: :string },
            _type: { type: :string },
            priority: { type: :string }
          }
        }

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
    end
  end
  
  #vote
  path 'issues/{id}/vote' do

    post 'Vote an specified issue' do
      security [Bearer: []]
      tags 'Interactions'
      description 'Not implemented yet.'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'ok' do
        schema type: :array,
        items: {
          properties: {
            title: { type: :string },
            description: { type: :string },
            status: { type: :string },
            _type: { type: :string },
            priority: { type: :string }
          }
        }

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
    end
  end
  
  #unvote
  path 'issues/{id}/unvote' do

    post 'Unvote an specified issue' do
      security [Bearer: []]
      tags 'Interactions'
      description 'Not implemented yet.'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :integer

      response '200', 'ok' do
        schema type: :array,
        items: {
          properties: {
            title: { type: :string },
            description: { type: :string },
            status: { type: :string },
            _type: { type: :string },
            priority: { type: :string }
          }
        }

        let(:id) { Issue.create(name: 'foo', status: 'bar', photo_url: 'http://example.com/avatar.jpg').id }
        run_test!
      end
      
      response '401', 'The authorization header is no present or is not valid.' do
        let(:issue) { { title: 'Dodo', description: 'available', status: 'something' } }
        run_test!
      end
    end
  end
  
end
